<?php
/*
 * Список маршрутов для раздела новостей
 */
use libs\Router;
/*
 * Пример создания маршрута
 */
//Router::set(
//		['news/{id}' => 'newslist'],
//		['test' => 'show'],
//		['id' => '[0-9]+']
//		);

Router::set('/','Index');
Router::set('/index','Index');

Router::set('/error/','Error');

Router::set('/news/add','news\AddEdit', 'add');
Router::set('/news/edit','news\AddEdit', 'edit');
Router::set('/news/delete','news\Delete');
Router::set('/news/','news\Index');


Router::set('/auth', 'Auth', 'auth');
Router::set('/logout', 'Auth', 'logout');