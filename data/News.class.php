<?php

namespace data;

class News {
	protected $id;
	
	protected $title;
	
	protected $announce;
	
	protected $content;
	
	public function __construct(int $id, string $title, string $announce, string $content)
	{
		$this->id = $id;
		$this->title = $title;
		$this->announce = $announce;
		$this->content = $content;
	}
	
	public function get(string $variable)
	{
		return $this->$variable;
	}
}
