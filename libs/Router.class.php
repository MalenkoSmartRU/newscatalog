<?php
/*
 * Отвечает за маршрутизацию
 */
namespace libs;

final class Router
{
	protected static $urn;

	protected static $path = null;
	
	protected static $routes = [];
	
	protected static $params = [];

	// Метод инициализации
	public static function init()
	{
        self::$urn = filter_input(INPUT_SERVER, 'REQUEST_URI');
		self::explodeUrn();
		self::setRoutes();
		self::route(self::getRouteId());
	}
	
	// Выделение из urn части необходимой для переадресации
	protected static function explodeUrn()
	{
		// Выделить только параметры передаваемые роутеру
		$path = preg_split('/\?.*=?.*|^\/+|\/+$/i', self::$urn, null, PREG_SPLIT_NO_EMPTY);

		if (count($path)) {
			self::$path = $path[0];
		}
	}
	
	// Установка обработчика
	protected static function set(string $route, string $action, string $method = 'show')
	{
		self::$routes[] = [
			'route' => $route,
			'controller' => $action,
			'method' => $method
		];
	}
	
	// Устанавливает все возможные маршруты
	protected static function setRoutes()
	{
		require_once __DIR__ . '/../routes.php';
	}
	
	// Получить id текущего маршрута
	protected static function getRouteId()
	{
		// Получить информацию о текущем пути
		$pathArray = preg_split('{/+}', self::$path, null, PREG_SPLIT_NO_EMPTY);
		$pathCount = count($pathArray);

		foreach (self::$routes as $routeId => $route) {
			$routeArray = preg_split('{/+}', $route['route'], null, PREG_SPLIT_NO_EMPTY);
			$routeCount = count($routeArray);

			// Если длина пути и длина маршрута равны нулю
			if (($pathCount === 0) && ($routeCount === 0)) {
				return $routeId;
				
			// Если длина маршрута больше или равна длине маршрута шаблона и длина маршрута больше 0
			} elseif (($pathCount >= $routeCount) && ($routeCount > 0)) {
				
				// Указывает на наличие совпадений
				$valid = true;
				foreach ($routeArray as $patternElemId => $patternElem) {

					// Если элемент маршрута не равен элементу пути, перейти к проверке следующего маршрута
					if ($patternElem !== $pathArray[$patternElemId]) {
						$valid = false;
						break;
					}
				}

				// Если маршрут прошел проверку, вернуть его значение
				if (($valid === true)) {
					self::getParams($pathArray, $routeCount);
					return $routeId;
				}
				
			}
		}
		
		return null;
	}
	
	// Получить параметры маршрута
	protected static function getParams($array, $offset)
	{
		self::$params = array_slice($array, $offset);
	}
	
	// Подключение необходимого обработчика
	protected static function route($routeId)
	{
		$path = '\\controllers\\';
		if (is_null($routeId)) {
			$controllerPath = $path.'Error';
			$method = 'show';
		} else {
			$controllerPath = $path.self::$routes[$routeId]['controller'];
			$method = self::$routes[$routeId]['method'];
		}

		// Подключение обработчика
		(new $controllerPath)->$method();
	}
	
	// Вернуть значение параметра
	public static function param($id)
	{
        if (isset(self::$params[$id])) {
            return self::$params[$id];
        } else {
            return null;
        }
	}
    
    // Вернуть текущий путь
    public static function path()
    {
        return self::$path;
    }
}