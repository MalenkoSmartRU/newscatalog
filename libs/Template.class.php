<?php

namespace libs;

use libs\Router;
use libs\User;

/**
 * Предназначен для отображения шаблонов
 */
final class Template {
	protected static $isInit = false;
	
	protected static $path = null;
    
    protected static $glob = [];
	
	protected static $params = [];
	
	// Инициализация шаблонизатора
	public static function init($file)
	{
        self::setGlob();
		self::$path = __DIR__ . '/../templates/' . $file . '.tpl';
		self::$isInit = true;
	}
    
    // Установка глобальных значений
    protected static function setGlob()
    {
        self::$glob['path'] = Router::path();
        self::$glob['logged'] = User::logged();
    }
    
    // Получить глобальное значение
    public static function glob($name)
    {
        if (isset(self::$glob[$name])) {
            return self::$glob[$name];
        } else {
            return null;
        }
    }
	
	// Отобразить текущий шаблон
	public static function show($file = null)
	{
		if (!self::$isInit) {
			self::init($file);
		}
		
		include_once self::$path;
	}
	
	// Подключает шаблоны из папки "общие"
	protected static function common($file)
	{
		include_once __DIR__ . '/../templates/commons/' . $file . '.tpl';
	}
	
	// Устанавливает или возвращает параметры
	public static function param(string $name, $value = null)
	{
		if (isset(self::$params[$name])) {
			return self::$params[$name];
		} else {
			self::$params[$name] = $value;
		}
	}
	
	// Устанавливает массив параметров
	public static function params(array $params)
	{
		self::$params = array_merge(self::$params, $params);
	}
}