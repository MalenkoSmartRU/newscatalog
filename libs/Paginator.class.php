<?php
	namespace libs;

	// Отвечает за отображение пейджинатора
	class Paginator
	{
		// Общее количество страниц
		protected $count;
		// Текущая страница
		protected $current;
		// URI для навигации
		protected $uri;

		public function __construct(int $count, int $current, string $uri)
		{
			$this->count = $count;
			$this->current = $current;
			$this->uri = $uri;
		}
		
		public function show()
		{
			require_once __DIR__ . '/../templates/paginator.tpl';
		}
	}
