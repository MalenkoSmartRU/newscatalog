<?php
/*
 * Функция переадресации в случае ошибки
 */

function pageNotFound()
{
    exit(header('Location: /error'));
}

function go($path = null, $back = false)
{
    if ($back) {
        $path = filter_input(INPUT_SERVER, 'HTTP_REFERER');
    }
    
    if (is_null($path)) {
        $path = '/';
    }
    
    header('Location: '.$path);
}