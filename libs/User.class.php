<?php
/*
 * Инициализация нового пользователя
 */
namespace libs;

// Класс, 
final class User {
    // Авторизован ли пользователь
    protected static $logged = false;
    
    protected static $pass;
    
    
    
    public static function init($pass)
    {
        self::$pass = $pass;
       
        // Новая сессия
        $sessPath = '../sess';
        
        if (!file_exists($sessPath)) {
            mkdir($sessPath);
        }
        
        session_start([
            'name'      => 'SESS',
            'save_path' => $sessPath
        ]);
        
        // Определяем параметр авторизации
        self::setLogged();
    }
    
    protected static function setLogged()
    {
        if (isset($_SESSION['logged'])) {
            self::$logged = $_SESSION['logged'];
        } else {
            $_SESSION['logged'] = self::$logged;
        }
    }
    
    public static function logged()
    {
        return self::$logged;
    }
    
    public static function auth($pass)
    {
        if ($pass === self::$pass) {
            $_SESSION['logged'] = self::$logged = true;
        }
        
        return self::$logged;
    }
    
    public static function logout()
    {
        session_destroy();
    }
}