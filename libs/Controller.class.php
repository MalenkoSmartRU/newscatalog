<?php
/*
 * Базовый класс контроллера
 */
namespace libs;

use libs\User;

abstract class Controller
{
	protected static $db;

	public function __construct()
	{
		$config = require_once __DIR__ . '/../config.php';
		
		// Подключение к базе данных
		self::$db = new \PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'], $config['db_user'], $config['db_password']);
		self::$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		
        // Идентификация нового пользователя
        User::init($config['user_password']);
        
        // Вызов метода инициализации каждого контроллера
		$this->init();
	}
	
    // Метод инициализации, служит для перегрузки в дочерних классах
	protected function init() {}
}
