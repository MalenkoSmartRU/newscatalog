<?php
/*
 * Экземпляр контроллера
 */
namespace controllers;

use libs\Controller;
use libs\Template;
use libs\Validator;
use data\News;
use libs\Paginator;

class Index extends Controller
{
	protected $limit = 10; // Количество элементов на страницу
	
	protected $news = [];
    
    // Количество страниц
    protected $count;
    
    // Текущая страница
    protected $current;
    
    protected $paginator;
    
	protected function init()
	{
        $this->setCount();
        $this->setCurrent();
        $this->setPaginator();
		$this->setNews();
	}
	
	public function show()
	{
		Template::param('news', $this->news);
        Template::param('paginator', $this->paginator);
		Template::show('index');
	}
    
    // Настройка количества записей
    protected function setCount()
    {
        // Получить количество записей в таблице news
        $this->count = ceil((int)self::$db->query('SELECT COUNT(*) AS count FROM news')->fetch()['count'] / $this->limit);
    }
	
    // Определение текущей страницы
    protected function setCurrent()
    {
        // Получить id текущей страницы
        $validate = new Validator(INPUT_GET);
        if ($validate->validate('p', '/^[0-9]+$/') && (int)$validate->validated('p') <= $this->count) {
            $this->current = (int)$validate->validated('p');
        } else {
            $this->current = 1;
        }
    }
    
	// Установка новости
	protected function setNews()
	{
        // Получить все новости из базы данных в количестве :limit
		$request = self::$db->prepare('SELECT id_news AS id, title, announce, content FROM news LIMIT :count, :limit');
        
        $limit = $this->current * $this->limit - $this->limit;
        
        $request->bindParam(':count', $limit, \PDO::PARAM_INT);
		$request->bindParam(':limit', $this->limit, \PDO::PARAM_INT);
		$request->execute();

		foreach ($request->fetchAll() as $news) {
			$this->news[] = new News($news['id'], $news['title'], $news['announce'], $news['content']);
		}
        
        // Настраиваем пейджинатор
        $this->setPaginator();
	}
    
    // Настройка пейджинатора
    protected function setPaginator()
    {
        $this->paginator = new Paginator($this->count, $this->current, '?p=');
    }
}