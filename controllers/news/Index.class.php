<?php
/*
 * Экземпляр контроллера
 */
namespace controllers\news;

use libs\Controller;
use libs\Template;
use libs\Router;

class Index extends Controller
{	
    protected $id;
    
    protected $title;
    
    protected $announce;
    
    protected $content;
    
    protected function init()
    {
        $this->getNewsId();
        $this->setNews();
    }
    
	public function show()
	{
        Template::param('news', $this->data());
        Template::param('edited', (bool)filter_input(INPUT_GET, 'edited'));
		Template::show('news/index');
	}
    
    public function setNews()
    {
        self::$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_NUM);
        $request = self::$db->prepare('SELECT title, announce, content FROM news WHERE id_news = :id');
        $request->bindParam(':id', $this->id);
        $request->execute();
        
        $news = $request->fetch();
        
        if (!$news) {
            pageNotFound();
            
            return false;
        }
        
        list($this->title, $this->announce, $this->content) = $news; 
        
        return true;
    }
    
    // Получить идентификатор новости
    protected function getNewsId()
    {
        if (preg_match('{^[1-9]+[0-9]*$}', Router::param(0))) {
            $this->id = Router::param(0);
        } else {
            pageNotFound();
        }
    }
    
    // Вернуть массив параметров
    protected function data()
    {
        return [
            'id'        => $this->id,
            'title'     => $this->title,
            'announce'  => $this->announce,
            'content'   => $this->content
        ];
    }
}
