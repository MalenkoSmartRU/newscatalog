<?php
/*
 * Экземпляр контроллера
 */
namespace controllers\news;

use libs\Controller;
use libs\Template;
use libs\Errors;
use libs\User;

class Delete extends Controller
{
    protected $id;
    
    protected $sure;
    
    protected $errors;
    
    protected function init()
    {
        if (!User::logged()) {
            pageNotFound();
            return;
        }
        
        $this->errors = new Errors;
        $this->setParams();
        
        if ($this->sure && $this->id) {
            $this->delete();
        }
    }
    
	public function show()
	{
		Template::show('news/delete');
	}
    
    protected function setParams()
    {
        // Получить новости для удаления
        $id = filter_input(INPUT_GET, 'id');
        if (preg_match('{^[1-9]+[0-9]*$}', $id)) {
            // Проверить наличие данной новости в базе данных
            $request = self::$db->prepare('SELECT * FROM news WHERE id_news = :id');
            $request->bindParam(':id', $id);
            $request->execute();

            $data = $request->fetch();
            
            if ($data) {
                $this->id = (int)$id;
                Template::param('errors', null);
                Template::param('id', $this->id);
                Template::param('title', $data['title']);
                Template::param('no', filter_input(INPUT_SERVER, 'HTTP_REFERER'));
            } else {
               // pageNotFound();
                return;
            }
        } else {
           // pageNotFound();
            return;
        }
        
        // Подтверждение удаления новости
        $sure = filter_input(INPUT_GET, 'sure');
        if (preg_match('{^true$}', $sure)) {
            $this->sure = (bool)$sure;
            Template::param('sure', 'true');
        } else {
            Template::param('sure', 'false');
        }
    }
    
    protected function delete()
    {
        $request = self::$db->prepare('DELETE FROM news WHERE id_news = :id');
        $request->bindParam(':id', $this->id);
        $request->execute();
        
        go('/');
    }
}