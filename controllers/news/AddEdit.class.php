<?php
/*
 * Экземпляр контроллера
 */
namespace controllers\news;

use libs\Controller;
use libs\Template;
use libs\Errors;
use libs\User;
use controllers\news\Index;

class AddEdit extends Controller
{
    protected $id;
    protected $title;
    protected $announce;
    protected $content;
    protected $errors;
    
    protected function init()
    {
        if (!User::logged()) {
            pageNotFound();
            return;
        }
        
        $this->errors = new Errors();
    }
    
    protected function setValues()
    {
        if (!count($_POST)) {
            return false;
        }
        
        $title = filter_input(INPUT_POST, 'title');
        Template::param('title', htmlspecialchars($title));
        if (preg_match('{^.{5,120}$}usmi', $title)) {
            $this->title = $title;
        } else {
            $this->errors->add('Неверно введен заголовок новости (5-120 символов)');
        }
        
        $announce= filter_input(INPUT_POST, 'announce');
        Template::param('announce', htmlspecialchars($announce));
        if (preg_match('{^.{10,500}$}usmi', $announce)) {
            $this->announce = $announce;
        } else {
            $this->errors->add('Неверно заполнен анонс новости (10-500 символов)');
        }
        
        $content= filter_input(INPUT_POST, 'content');
        Template::param('content', htmlspecialchars($content));
        if (preg_match('{^.{10,}$}usmi', $content)) {
            $this->content = $content;
        } else {
            $this->errors->add('Неверно заполнено содержимое новости (10-500 символов)');
        }
        
        Template::param('errors', $this->errors->messages());

        if ($this->errors->count()) {
            return false;
        } else {
            return true;
        }
    }


    public function add()
    {
        $this->action('add');
        Template::show('news/add');
    }
    
    public function edit()
    {
        $this->getNewsId();
        $this->setNews();
        
        $this->action('edit');
        Template::param('news', $this->data());
        Template::show('news/edit');
    }
    
    public function action($action)
    {
        if ($action === 'add') {
            $query = 'INSERT INTO news (title, announce, content) VALUES (:title, :announce, :content)';
        } elseif ($action === 'edit') {
            $query = 'UPDATE news SET title = :title, announce = :announce, content = :content WHERE id_news = :id';
        }

        if ($this->setValues())
        {
            $request = self::$db->prepare($query);
            $request->bindParam(':title', $this->title, \PDO::PARAM_STR);
            $request->bindParam(':announce', $this->announce, \PDO::PARAM_STR);
            $request->bindParam(':content', $this->content, \PDO::PARAM_STR);
            if ($action === 'edit') {
             $request->bindParam(':id', $this->id, \PDO::PARAM_INT);
            }

            $request->execute();
            
            if ($action === 'add') {
                $this->id = self::$db->lastInsertId();
                go('/news/'.$this->id);
            } elseif ($action === 'edit') {
                go('/news/'.$this->id.'?edited=true');
            }
            
        }
    }
    
    public function setNews()
    {
        self::$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_NUM);
        $request = self::$db->prepare('SELECT title, announce, content FROM news WHERE id_news = :id');
        $request->bindParam(':id', $this->id);
        $request->execute();
        
        $news = $request->fetch();
        
        if (!$news) {
            pageNotFound();
            
            return false;
        }
        
        list($this->title, $this->announce, $this->content) = $news; 
        
        return true;
    }
    
    // Получить идентификатор новости
    protected function getNewsId()
    {
        $id = filter_input(INPUT_GET, 'id');
        if (preg_match('{^[1-9]+[0-9]*$}', $id)) {
            $this->id = (int)$id;
            Template::param('errors', null);
            Template::param('id', $this->id);
            Template::param('no', filter_input(INPUT_SERVER, 'HTTP_REFERER'));
        } else {
            pageNotFound();
        }
    }
    
    // Вернуть массив параметров
    protected function data()
    {
        return [
            'id'        => $this->id,
            'title'     => $this->title,
            'announce'  => $this->announce,
            'content'   => $this->content
        ];
    }
}