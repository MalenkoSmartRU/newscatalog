<?php
/*
 * Экземпляр контроллера
 */
namespace controllers;

use libs\Controller;
use libs\Template;

class Error extends Controller
{
	public function show()
	{
		Template::show('error');
	}
}