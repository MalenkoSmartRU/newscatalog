<?php
/*
 * Экземпляр контроллера
 */
namespace controllers;

use libs\Controller;
use libs\Template;
use libs\User;
use libs\Errors;

class Auth extends Controller
{    
	public function show()
	{
		Template::show('auth');
	}
    
    public function auth()
    {
        if (User::logged()) {
            pageNotFound();
        }
        
        // Подключить класс ошибок
        $errors = new Errors;
        
        $pass = filter_input(INPUT_POST, 'password');
        
        // Попытаться выполнить авторизацию
        if (User::auth($pass)) {
            go('/');
        } else {
            // Если пароль был введен и не совпал, добавить ошибку
            if ($pass) {
                $errors->add('Пароли не совпадают!');
            }
        }
        
        Template::param('errors', $errors->messages());
        $this->show();
    }
    
    public function logout()
    {
        User::logout();
        go('/');
    }
}