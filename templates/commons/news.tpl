<section class="news">
	<h2 class="news--title">Список новостей</h2>
	
	<?php foreach (self::param('news') as $news):?>
	<section class="news--public">
		<h3 class="news--public__title"><a href="/news/<?=$news->get('id')?>" class="news--public__link"><?=$news->get('title')?></a></h3>
		<div class="news--public__announce"><?=$news->get('announce')?></div>
        <?php if (self::glob('logged')): ?>
        <div class="news--public__edit">
            <ul>
                <li><a href="/news/edit?id=<?=$news->get('id')?>">Изменить</a></li>
                <li><a href="/news/delete?id=<?=$news->get('id')?>">Удалить</a></li>
            </ul>
        </div>
        <?php endif ?>
	</section>
	<?php endforeach;?>
</section>