<header class="header">
	<div class="content header--content block">
		<h1 class="header--title">Каталог новостей</h1>
	</div>
    <div class="header--auth content block">
        <?php if (self::glob('logged')):?>
        Вы вошли как администратор | <a href="/news/add">Добавить новость</a> | <a href="/logout">Выйти</a>
        <?php else: ?>
            <a href="/auth" class="header--auth__link">Войти как администратор</a>
        <?php endif; ?>
    </div>
</header>