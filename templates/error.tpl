<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>Каталог новостей</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
		<main class="main">
			<div class="main-content content block">
                <div class="pagenotfound">
                    Такой страницы не существует! <a href="/">Вернуться на главную</a>
                </div>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
