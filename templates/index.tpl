<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>Каталог новостей</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
		<main class="main">
			<div class="main-content content block">
				<?php self::common('news');?>
                <?php self::param('paginator')->show()?>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
