<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>Страница авторизации</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
        <?php if (self::glob('path')):?>
        <nav class="nav content block">
            <ul class="nav-list">
                <li><a href="/">Главная</a></li>
                <li> > </li>
                <li>Авторизация</li>
            </ul>
        </nav>
        <?php endif; ?>
		<main class="main">
			<div class="main--content content block">
                <?php if (self::param('errors')):?>
                    <div class="errors">
                        <?php foreach(self::param('errors') as $error):?>
                            <?=$error?>
                        <?php endforeach;?>
                    </div>
                <?php endif;?>
                <form method="post" class="auth">
                    <input type="password" placeholder="Введите ваш пароль" name="password" class="auth--password">
                    <input type="submit" value="Войти" class="auth--submit">
                </form>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
