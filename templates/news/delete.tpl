<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>Удаление новости "<?=self::param('title')?>"</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
        <?php if (self::glob('path')):?>
        <nav class="nav content block">
            <ul class="nav-list">
                <li><a href="/">Главная</a></li>
                <li> > </li>
                <li>Подтвердите удаление новости "<?=self::param('title')?>"</li>
            </ul>
        </nav>
        <?php endif; ?>
		<main class="main">
			<div class="main--content content block">
                <div class="big">Вы дейтвительно хотите удалить новость "<?=self::param('title')?>" (номер <?=self::param('id')?>)?</div>
                <div class="news--delete">
                    <a href="/news/delete?id=<?=self::param('id')?>&sure=true">Да</a>
                    <a href="<?=self::param('no')?>">Нет</a>
                </div>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>