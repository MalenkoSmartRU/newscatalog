<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>TODO supply a title</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
        <?php if (self::glob('path')):?>
        <nav class="nav content block">
            <ul class="nav-list">
                <li><a href="/">Главная</a></li>
                <li> > </li>
                <li>Добавить новость</li>
            </ul>
        </nav>
        <?php endif; ?>
		<main class="main">
			<div class="main--content content block">
                <?php if (self::param('errors')):?>
                    <ul class="errors">
                        <?php foreach(self::param('errors') as $error):?>
                        <li><?=$error?></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
                <form method="post" class="news--add">
                    <input name="title" placeholder="Заголовок новости" class="news--add__title" value="<?=self::param('title')?>">
                    <input name="announce" placeholder="Анонс новости" class="news--add__announce" value="<?=self::param('announce')?>">
                    <textarea name="content" placeholder="Текст новости" class="news--add__content"><?=self::param('content')?></textarea>
                    <input type="submit" value="Добавить новость!" class="news--add__submit">
                </form>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
