<!DOCTYPE html>
<html lang="ru">
	<head>
		<title>Редактирование новости "<?=self::param('news')['title']?>"</title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
        <?php if (self::glob('path')):?>
        <nav class="nav content block">
            <ul class="nav-list">
                <li><a href="/">Главная</a></li>
                <li> > </li>
                <li><a href="/news/<?=self::param('news')['id']?>"><?=self::param('news')['title']?></a></li>
                <li> > </li>
                <li>Изменение новости</li>
            </ul>
        </nav>
        <?php endif; ?>
		<main class="main">
			<div class="main--content content block">
                <?php if (self::param('errors')):?>
                    <ul class="errors">
                        <?php foreach(self::param('errors') as $error):?>
                        <li><?=$error?></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
                <form class="news news--add" method="post">
                    <input class="news--add__title" value="<?=self::param('news')['title']?>" name="title">
                    <input class="news--add__announce" value="<?=self::param('news')['announce']?>" name="announce">
                    <textarea class="news--add__content" name="content"><?=self::param('news')['content']?></textarea>
                    <input type="submit" value="Изменить новость!" class="news--add__submit">
                </form>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
