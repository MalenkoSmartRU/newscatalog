<!DOCTYPE html>
<html lang="ru">
	<head>
		<title><?=self::param('news')['title']?></title>
		<?php self::common('includes');?>
	</head>
	<body>
		<?php self::common('header');?>
        <?php if (self::glob('path')):?>
        <nav class="nav content block">
            <ul class="nav-list">
                <li><a href="/">Главная</a></li>
                <li> > </li>
                <li><?=self::param('news')['title']?></li>
            </ul>
        </nav>
        <?php endif; ?>
		<main class="main">
			<div class="main--content content block">
                <article class="news">
                    <figure>
                    <h2 class="news--title"><?=self::param('news')['title']?></h2>
                     <?php if (self::param('edited')): ?>
                       <div class="news--info">Новость успешно отредактирована!</div>
                    <?php endif ?>
                    <figcaption class="news--announce"><?=self::param('news')['announce']?></figcaption>
                    </figure>
                    <div class="news--content"><?=self::param('news')['content']?></div>
                    <?php if(self::glob('logged')):?>
                    <ul class="news--controls">
                        <li><a href="/news/edit?id=<?=self::param('news')['id']?>">Изменить</a></li>
                        <li><a href="/news/delete?id=<?=self::param('news')['id']?>">Удалить</a></li>
                    </ul>
                    <?php endif;?>
                </article>
			</div>
		</main>
		<?php self::common('footer');?>
	</body>
</html>
