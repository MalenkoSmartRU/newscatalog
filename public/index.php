<?php
/*
 * Фронт контроллер каталога новостей
 */

use libs\Router;

// Функции отладки
ini_set('display_errors', 'On');
error_reporting(E_ALL);

 // Подключение автозагрузчика
 require_once __DIR__ . '/../autoloader.php';
 // Подключение функций
 require_once __DIR__ . '/../libs/functions.php';
 
 //Инициализация маршрутизатора
Router::init();