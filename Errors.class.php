<?php
	namespace libs;
	
	// Класс хранения информации об ошибках
	class Errors
	{
		protected $messages = [];
		protected $count = 0;
		
		// Добавление новой ошибки
		public function add($message)
		{
			$this->messages[] = $message;
			$this->count += 1;
		}
		
		// Методы возвращающие значения
		public function getMessages()
		{
			return $this->messages;
		}
		
		public function getCount()
		{
			return $this->count;
		}
	}
